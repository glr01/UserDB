const { v4: uuid } = require("uuid")

const {
  getUserObject,
  encryptPassword,
  decryptPassword,
  getVectorAndKey
} = require("../utils/helper")

const User = require("../models/User")

module.exports = {
  async store(request, response) {
    const { email, password } = request.body
    const [initVector, securityKey] = getVectorAndKey()

    let user = await User.findOne({ email })

    if (!user) {
      const token = uuid()
      const hashedPwd = encryptPassword(password, securityKey, initVector)

      user = await User.create({
        token,
        initVector,
        securityKey,
        email,
        password: hashedPwd
      })

      return response.json(getUserObject(user))
    }

    return response.status(403).json({ error: "Email address already in use." })
  },

  async update(request, response) {
    const { email, password } = request.body
    const [initVector, securityKey] = getVectorAndKey()
    const hashedPwd = encryptPassword(password, securityKey, initVector)

    let user = await User.findOne({ email })

    if (user) {
      // await user.updateOne({ password: hashedPwd, initVector, securityKey })
      user.password = hashedPwd
      user.initVector = initVector
      user.securityKey = securityKey
      await user.save()

      return response.json(getUserObject(user))
    }

    return response.status(404).json({
      error: "Could not find an account associated with email address."
    })
  },

  async login(request, response) {
    const { email, password } = request.body

    const user = await User.findOne({ email })

    if (user) {
      const { password: hashedPwd, securityKey, initVector } = user
      const decryptPwd = decryptPassword(hashedPwd, securityKey, initVector)

      const matchesPwd = password === decryptPwd

      if (matchesPwd) return response.json(getUserObject(user))

      return response.status(401).json({ error: "Password incorrect." })
    }

    return response.status(404).json({
      error: "Could not find an account associated with email address."
    })
  }
}
