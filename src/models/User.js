const mongoose = require("mongoose")

const UserSchema = new mongoose.Schema({
  token: String,
  initVector: Buffer,
  securityKey: Buffer,

  email: {
    type: String,
    required: true
  },

  password: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model("User", UserSchema)
