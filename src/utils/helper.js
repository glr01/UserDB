const crypto = require("crypto")

function encryptPassword(password, securityKey, initVector) {
  const cipher = crypto.createCipheriv("aes-256-gcm", securityKey, initVector)
  let hashedPwd = cipher.update(password, "utf-8", "hex")

  return (hashedPwd += cipher.final("hex"))
}

function decryptPassword(hashedPwd, securityKey, initVector) {
  const decipher = crypto.createDecipheriv(
    "aes-256-gcm",
    securityKey,
    initVector
  )

  const decryptPwd = decipher.update(hashedPwd, "hex", "utf-8")

  return decryptPwd
}

function getVectorAndKey() {
  const initVector = crypto.randomBytes(16)
  const securityKey = crypto.randomBytes(32)

  return [initVector, securityKey]
}

function getUserObject(user) {
  return {
    token: user.token,
    email: user.email,
    password: user.password
  }
}

module.exports = {
  getUserObject,
  encryptPassword,
  decryptPassword,
  getVectorAndKey
}
