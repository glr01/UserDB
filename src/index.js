require("dotenv").config()

const cors = require("cors")
const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")

const routes = require("./routes")

const app = express()

app.use(
  cors({
    origin: "*"
  })
)
app.use(bodyParser.json())
app.use(routes)

mongoose.connect(process.env.MONGO_DB_URI)

app.listen(3333, () => console.log("Listening on port 3333"))
