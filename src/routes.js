const { Router } = require("express")
const AuthController = require("./controllers/AuthController")

const routes = Router()

routes.post("/login", AuthController.login)
routes.post("/signup", AuthController.store)
routes.put("/update", AuthController.update)

module.exports = routes
